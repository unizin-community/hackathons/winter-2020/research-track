#!/usr/bin/env perl

use Data::Dumper;
use DBI;
use Digest::MD5 qw(md5_hex);
use Getopt::Long;
use HTML::Strip;
use JSON;
use List::MoreUtils qw(uniq);
use Math::Round qw(nearest_floor);
use open ':std', ':encoding(UTF-8)';
use strict;
use Scalar::Util qw(looks_like_number);
use Statistics::Basic qw(mean stddev);
use Text::CSV;
use Time::Piece;
use Time::Seconds;
use utf8;
use warnings;
use YAML;
use YAML::XS 'LoadFile';

## Root dir
my $d_root = '';
my $f_conf = $d_root . '/config.yaml';
my $config = LoadFile($f_conf);

## CSV object for parsing & writing.
my $csv =
  Text::CSV->new({ binary => 1, sep_char => ',' })
    or die "Cannot use CSV: ".Text::CSV->error_diag ();

## Metrics.
my @g_metrics = (
  'count_sessions',
  'count_submissions',
  'count_pageviews',
  'duration_sessions',
  'time_between_first_access_and_first_submission',
  'time_between_first_access_and_due_date',
  'time_between_last_submission_and_due_date'
);

foreach my $institution (keys $config) {

  print "Working on $institution\n";

  ##############################################################################
  ########### Verify that we have directories and files we need ################
  ##############################################################################

  ## Directory for this institution's input and output data.
  my $d_institution = $d_root . '/' . $institution;

  &check_dir($d_institution);

  ## Shortcuts for the input and output directories.
  my $d_output = $d_institution . '/output';
  my $d_input  = $d_institution . '/input';

  &check_dir($d_input);
  &check_dir($d_output);

  ## Define all of the input and output file paths.
  my $f_log     = $d_output . '/log.txt';
  my $i_survey  = $d_input  . '/survey.csv';
  my $i_mapping = $d_input  . '/mapping.csv';
  my $i_events  = $d_input  . '/events.csv';
  my $o_course  = $d_output . '/course.csv';
  my $o_student = $d_output . '/student.csv';
  my $o_student_course = $d_output . '/student_course.csv';

  ## Term ID for the courses from which we'll put user data.
  my $term_global_canvas_id = $config->{$institution}->{term_global_canvas_id};

  ## Create the log we'll use to track the generation
  ## of this dataset.
  open(my $fh_log, ">$f_log")
    or die "Couldn't create the log file: $f_log\n";

  print $fh_log "All work will be logged here.\n";

  ##############################################################################
  #################### Source dimensional data #################################
  ##############################################################################


  ## Input data from file.
  &print_header_1($fh_log, 'Load data from file...');

  ## Load the survey data from file. This is also the
  ## way we'll determine the canvas user ids whose data
  ## we aggregate.
  my $survey_data  = &load_survey_data_from_file($fh_log, $i_survey);
  my $mapping_data = &load_mapping_data_from_file($fh_log, $i_mapping);

  ## Input data from the Context store and UDW.
  &print_header_1($fh_log, 'Load data from db...');

  my $udw = &connect_to_database(
    $config->{$institution}->{udw}->{name},
    $config->{$institution}->{udw}->{host},
    $config->{$institution}->{udw}->{port},
    $config->{$institution}->{udw}->{user},
    $config->{$institution}->{udw}->{pass}
  );

  ## User & course data.
  my ($udw_user_data, $udw_course_data) =
    &load_udw_user_course_data($config, $udw, $term_global_canvas_id, $survey_data);

  &load_udw_course_data($config, $udw, $institution, $udw_course_data, $mapping_data);
  &load_udw_course_assignment_count($config, $udw, $institution, $udw_course_data);
  &load_udw_course_assignment_modules($config, $udw, $institution, $udw_course_data);
  &load_udw_course_assignment_discussions($config, $udw, $institution, $udw_course_data);
  &load_udw_course_assignment_quizzes($config, $udw, $institution, $udw_course_data);

  ## Assignment data.
  my $udw_assigment_data =
    &load_udw_assignment_data($config, $udw, $institution, $udw_course_data);

  &disconnect_from_database($udw);

  ## Prompt the user to go fetch UDP Event store data
  ## (this need be done one-time)
  &big_query_event_query($udw_course_data);

  ##############################################################################
  #################### Analyze the dimensional data ############################
  ##############################################################################
  &print_header_1($fh_log, 'Data analysis');

  &analyze_loaded_data(
    $fh_log, $survey_data, $udw_user_data, $udw_course_data, $udw_assigment_data
  );

  ##############################################################################
  #################### Prepare, load & process event data ######################
  ##############################################################################
  &print_header_1($fh_log, 'Prepare, load, and process events');

  ## A data structure to keep track of our event-sourced
  ## data and, eventually, our enrollment-level features.
  my $event_user_data =
    &set_event_user_data(
      $fh_log, $udw_user_data, $udw_course_data, $udw_assigment_data
    );

  ## Load events
  my @events = &load_events($i_events, $institution);

  ## Process events
  &process_events($fh_log, \@events, $event_user_data);

  ##############################################################################
  #################### Compute features ########################################
  ##############################################################################
  &print_header_1($fh_log, 'Compute statistics & features');

  ## Aggregate the metric distributions for
  ## users and courses.
  &metric_distributions($udw_user_data, $event_user_data, $udw_course_data, $udw_assigment_data);

  ## Compute the metric mean and standard deviations
  ## for our courses.
  &course_metric_statistics($udw_course_data);

  ## Compute the metric means for users and z-scores
  ## (but only for the students we're going to report).
  &user_metric_statistics($survey_data, $event_user_data, $udw_course_data);

  ##############################################################################
  #################### Output analysis artifacts. ##############################
  ##############################################################################
  &print_header_1($fh_log, 'Generate research dataset');

  ## Course measures file.
  &generate_course_results($fh_log, $institution, $o_course, $udw_course_data, $udw_assigment_data);

  ## Student measures file.
  &generate_student_results($fh_log, $institution, $o_student, $survey_data);

  ## Student-course measures file.
  &generate_student_course_results($fh_log, $institution, $o_student_course, $survey_data, $event_user_data);

  ## Clean up
  close($fh_log);
}

exit;

################################################################################
################################################################################
sub distribution_struct($) {
  my($data) = @_;

  $data->{distribution} = {};

  foreach my $g_metric (@g_metrics) {
    $data->{distribution}->{$g_metric} = [];
  }

  return 1;
}

sub add_feature_value($$$) {
  my($user_course_distribution, $course_distribution, $key, $value) = @_;

  if( $value ) {
    push($course_distribution->{$key}, $value);
    push($user_course_distribution->{$key}, $value);
  }

  return 1;
}

sub metric_distributions($$$$) {
  my($udw_user_data, $event_user_data, $udw_course_data, $udw_assigment_data) = @_;

  print "Computing metric distributions\n";

  ## By this point, we have all of our raw data points, and some
  ## aggregated data points, extracted or derived from source
  ## data. We now need to create the conditions to produce the
  ## features of the dataset.

  ## Iterate over every user of the student population.
  ##
  ## Only students who are part of the survey data will be
  ## included in the research dataset. But because the features
  ## contain population statistics, we need complete population
  ## data.
  my @user_canvas_ids = keys $event_user_data;

  foreach my $user_canvas_id (@user_canvas_ids) {

    ## Iterate over every course in which the student was
    ## enrolled during the term.
    my @course_canvas_ids = keys $event_user_data->{$user_canvas_id};

    foreach my $course_canvas_id (@course_canvas_ids) {

      ############################################################
      ## Course and user source data –– this is just for
      ##  convenience in computing the data later.
      ##
      my $course_data      = $udw_course_data->{$course_canvas_id};
      my $user_course_data = $event_user_data->{$user_canvas_id}->{$course_canvas_id};

      ##########################################
      ## Preparing for features computation.
      ##
      ## Create the data structures to house our time values
      ## from which we're compute averages and z-scores.
      ##
      &distribution_struct($course_data)      if not defined $course_data->{distribution};
      &distribution_struct($user_course_data) if not defined $user_course_data->{distribution};

      my $course_distribution      = $course_data->{distribution};
      my $user_course_distribution = $user_course_data->{distribution};

      ## Iterate over each assignment, since we have some
      ## features that measure assignment-related behaviors.
      my @assignment_canvas_ids = keys $user_course_data->{assignments};

      foreach my $assignment_canvas_id (@assignment_canvas_ids) {

        #######################################################
        ## Convenient to get the user's event-based assignment
        ## data in this course.
        my $assignment_data      = $udw_assigment_data->{$course_canvas_id}->{assignments}->{$assignment_canvas_id};
        my $user_assignment_data = $user_course_data->{assignments}->{$assignment_canvas_id};

        ## Due date for the assignment from the assignment data.
        ## If there is an override due date for this student, we
        ## don't yet respect it.
        my $due_date = $assignment_data->{due_date};

        ## What is the user's first navigation to this assignment?
        my $first_navigation =
          defined $user_assignment_data->{first_navigation} ?
            $user_assignment_data->{first_navigation} : undef;

        ## All of the distinct instances that the user
        ## submitted this assignment.
        my @submissions = @{$user_assignment_data->{submissions}};

        ## Sort the submission timestamps in ascending order.
        ## This means the first and last values of the array
        ## will be the first and last submissions, respectively.
        @submissions = sort { $a <=> $b } @submissions;

        ########################################################################
        ########### Features (assignment/submission derived) ###################
        ########################################################################

        ##########################
        ## Count-based features.
        my $count_submissions =
          defined $user_assignment_data->{submissions} ?
            scalar(@{$user_assignment_data->{submissions}}) : undef;

        &add_feature_value($user_course_distribution, $course_distribution, 'count_submissions', $count_submissions);

        #########################
        ## Time-based features.
        my $time_between_first_access_and_first_submission  = undef;
        my $time_between_first_access_and_due_date          = undef;
        my $time_between_last_submission_and_due_date       = undef;

        ## Compute time between first access and first submission.
        $time_between_first_access_and_first_submission = $submissions[0] - $first_navigation
          if $submissions[0] && $first_navigation;

        ## Compute time between first access and due date.
        $time_between_first_access_and_due_date = $due_date - $first_navigation
          if $due_date && $first_navigation;

        ## Compute time between last submission and due date.
        $time_between_last_submission_and_due_date = $due_date - $submissions[-1]
          if $due_date && $submissions[-1];

        ## Add these time-based values to the student and course
        ## averages if they exist.
        &add_feature_value(
          $user_course_distribution,
          $course_distribution,
          'time_between_first_access_and_first_submission',
          $time_between_first_access_and_first_submission
        );

        &add_feature_value(
          $user_course_distribution,
          $course_distribution,
          'time_between_first_access_and_due_date',
          $time_between_first_access_and_due_date
        );

        &add_feature_value(
          $user_course_distribution,
          $course_distribution,
          'time_between_last_submission_and_due_date',
          $time_between_last_submission_and_due_date
        );
      }

      ##########################################################################
      ################### Features (course-derived) ############################
      ##########################################################################

      ###############################
      ## Duration-based features.
      my @user_sessions     = keys $user_course_data->{sessions};

      foreach my $session_id (@user_sessions) {
        my $duration = undef;
        my $session  = $user_course_data->{sessions}->{$session_id};

        if( $session->{first} && $session->{last} ) {
          $duration = $session->{last} - $session->{first};
        }

        &add_feature_value($user_course_distribution, $course_distribution, 'duration_sessions', $duration);
      }

      ##########################
      ## Count-based features.
      ##
      my $count_sessions = defined $user_course_data->{sessions} ?
        scalar(@user_sessions) : undef;

      my $count_pageviews = defined $user_course_data->{count_pageviews} ?
        $user_course_data->{count_pageviews} : undef;

      &add_feature_value($user_course_distribution, $course_distribution, 'count_sessions', $count_sessions);
      &add_feature_value($user_course_distribution, $course_distribution, 'count_pageviews', $count_pageviews);
    }
  }

  return 1;
}

sub course_metric_statistics($$$$) {
  my($udw_course_data) = @_;

  print "Computing course features\n";

  ## Iterate over every course (and hence student
  ## population in the course).
  my @course_canvas_ids = keys $udw_course_data;

  foreach my $course_canvas_id (@course_canvas_ids) {

    ## Iterate over every feature_raw metric.
    my @features = keys $udw_course_data->{$course_canvas_id}->{distribution};

    foreach my $key (@features) {
      my $values = $udw_course_data->{$course_canvas_id}->{distribution}->{$key};

      ## Compute the mean and standard deviation for this
      ## stat given the available population data.
      my $mean   = mean($values);
      my $stddev = stddev($values);

      $udw_course_data->{$course_canvas_id}->{statistics}->{"mean_${key}"} =
        defined $mean ? $mean : undef;

      $udw_course_data->{$course_canvas_id}->{statistics}->{"stddev_${key}"} =
        defined $stddev ? $stddev : undef;
    }
  }

  return 1;
}

sub user_metric_statistics($$$) {
  my($survey_data, $event_user_data, $udw_course_data) = @_;

  print "Computing features (z-scores)\n";

  ## Iterate over every user of the student population.
  ##
  my @user_canvas_ids = keys $survey_data;

  foreach my $user_canvas_id (@user_canvas_ids) {

    ## Iterate over every course in which the student was
    ## enrolled during the term.
    my @course_canvas_ids = keys $event_user_data->{$user_canvas_id};

    foreach my $course_canvas_id (@course_canvas_ids) {

      ############################################################
      ## Course and user source data –– this is just for
      ##  convenience in computing the data later.
      ##
      my $course_data      = $udw_course_data->{$course_canvas_id};
      my $user_course_data = $event_user_data->{$user_canvas_id}->{$course_canvas_id};

      ## Create the data structure for the z-scores
      ## of this user in this course.
      $user_course_data->{zscores} = {};

      ## Iterate over every key for which we generate a z-score.
      my @features = keys $user_course_data->{distribution};

      foreach my $key (@features) {

        my $pop_mean   = $udw_course_data->{$course_canvas_id}->{statistics}->{"mean_${key}"};
        my $pop_stddev = $udw_course_data->{$course_canvas_id}->{statistics}->{"stddev_${key}"};

        my $user_distribution = $user_course_data->{distribution}->{$key};

        ## Compute the mean and standard deviation for this
        ## stat given the available population data.
        my $user_mean = mean($user_distribution);

        ## Set a default value for the z-score.
        my $zscore = undef;

        ## Compute the z-score.
        $zscore = ($user_mean - $pop_mean) / $pop_stddev
          if $user_mean ne 'n/a' && $pop_mean ne 'n/a'  && $pop_stddev ne 'n/a';

        ## Keep the z-score.
        $user_course_data->{zscores}->{$key} = $zscore;
      }
    }
  }

  return 1;
}

################################################################################
################################################################################

sub load_survey_data_from_file($$) {
  my($fh_log, $i_file) = @_;
  my $data = {};

  open(my $fh, '<', $i_file)
    or die "Couldn't open file: $i_file.\n";

  while (my $line = <$fh>) {
    if( $csv->parse($line) ) {

      my @fields = $csv->fields();

      my $user_canvas_id = $fields[0];
      my $assist_ac      = $fields[1];
      my $assist_os      = $fields[2];

      $data->{$user_canvas_id} = {
        assist_ac => $assist_ac,
        assist_os => $assist_os
      };
    }
  }

  close($fh);

  return $data;
}

sub load_mapping_data_from_file($$) {
  my($fh_log, $i_file) = @_;
  my $data = {};

  open(my $fh, '<', $i_file)
    or die "Couldn't open file: $i_file.\n";

  while (my $line = <$fh>) {
    if( $csv->parse($line) ) {

      my @fields = $csv->fields();

      my $subject  = $fields[0];
      my $category = $fields[1];

      $data->{$subject} = $category;
    }
  }

  close($fh);

  return $data;
}

sub create_if_not_user($$$$) {
  my($data, $user_canvas_id, $user_canvas_global_id, $name) = @_;

  $data->{$user_canvas_id} = {
    user_canvas_global_id => $user_canvas_global_id,
    name => $name
  } if ! defined $data->{$user_canvas_id};

  return $data->{$user_canvas_id};
}

sub load_udw_user_course_data($$$) {
  my($config, $db, $term_id, $survey_data) = @_;
  my $udw_user_data   = {};
  my $udw_course_data = {};

  print "Getting user & course data...";

  #########################################################
  ## Step 1:
  ##
  ## Get all of the courses for the survey users.
  ##
  ## That will create the base of courses from which
  ## we'll need to get all course enrollment data.
  ##
  ## User string.
  my @user_canvas_ids       = keys $survey_data;
  my $user_canvas_id_string = join (',', @user_canvas_ids);

  my $query = <<"END_OF_SQL";
SELECT
  distinct(c.canvas_id) course_canvas_id
FROM
  enrollment_dim cse
INNER JOIN
  course_section_dim cs ON cse.course_section_id=cs.id
INNER JOIN
  course_dim c ON cs.course_id=c.id
INNER JOIN
  enrollment_term_dim t ON cs.enrollment_term_id=t.id
INNER JOIN
  role_dim r ON cse.role_id=r.id
INNER JOIN
  user_dim u ON cse.user_id=u.id
WHERE
  u.canvas_id IN ($user_canvas_id_string)
  AND cse.workflow_state in ('active', 'completed')
  AND r.base_role_type = 'StudentEnrollment'
  AND c.workflow_state in ('available', 'completed')
  AND t.id = $term_id
END_OF_SQL

  my @course_canvas_ids = ();

  my $sth = $db->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {
    push(@course_canvas_ids, $row->{course_canvas_id});
  }

  $sth->finish;

  #########################################################
  ## Step 2:
  ##
  ## Get all of the course section enrollments for the
  ## survey users in the academic term we care about.
  ##
  my $course_canvas_ids_string = join(',', @course_canvas_ids);

  $query = <<"END_OF_SQL";
SELECT
  u.canvas_id user_canvas_id
  , c.canvas_id course_canvas_id
  , u.id user_canvas_global_id
  , u.name user_name
FROM
  enrollment_dim cse
INNER JOIN
  course_section_dim cs ON cse.course_section_id=cs.id
INNER JOIN
  course_dim c ON cs.course_id=c.id
INNER JOIN
  user_dim u ON cse.user_id=u.id
WHERE
  cse.workflow_state in ('active', 'completed')
  AND c.canvas_id IN ($course_canvas_ids_string)
  AND c.workflow_state in ('available', 'completed')
END_OF_SQL

  $sth = $db->prepare($query);
  $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {

    ## Extract the user and course identifiers.
    my $user_canvas_id   = $row->{user_canvas_id};
    my $course_canvas_id = $row->{course_canvas_id};

    ## Create the user record if needed.
    my $user_data = &create_if_not_user(
      $udw_user_data,
      $user_canvas_id,
      $row->{user_canvas_global_id},
      $row->{user_name}
    );

    ## Create a data structure for this user's
    ## course enrollments.
    $user_data->{courses} = []
      if ! defined $user_data->{courses};

    ## Push this course ID on the student's list of
    ## course enrollments.
    push($user_data->{courses}, $course_canvas_id);

    ## Make sure that we add a unique course id to the
    ## current list of course ids;
    my @current = uniq @{$user_data->{courses}};
    $user_data->{courses} = \@current;

    ## This is a hack, since we repeat this step
    ## for each enrollment of a course.
    ##
    ## Start to create the course data structure, which
    ## we'll complete with more data in another set of
    ## queries in load_udw_course_data().
    $udw_course_data->{$course_canvas_id} = {
      name                              => undef,
      course_level                      => undef,
      actual_student_enrollments        => 0,
      rounded_student_enrollments       => 0,
      course_category                   => undef,
      actual_count_assignments          => 0,
      rounded_count_assignments         => 0,
      percentage_discussion_assignments => 0,
      percentage_quiz_assignments       => 0,
      assignments_in_modules            => 0
    };
  }

  $sth->finish;

  print "... done\n";

  return ($udw_user_data, $udw_course_data);
}

sub load_udw_course_data($$$$$) {
  my($config, $db, $institution, $udw_course_data, $mapping_data) = @_;

  print "Getting course data...";

  ## Unique set of course ids for the academic term.
  my $course_str = join(', ', keys $udw_course_data);

  ## To do:
  ##   * Handle case with no courses.

  #########################################################
  ## Get all of the course section enrollments for these
  ## users that are in the academic term we care about.
  ##
  ## Title, enrollments, course_level.
  my $query = <<"END_OF_SQL";
  SELECT
    c.canvas_id course_canvas_id
    , count(cse.id) count_student_enrollments
    , c.name course_name
    , c.code course_code
  FROM
    enrollment_dim cse
  INNER JOIN
    course_section_dim cs ON cse.course_section_id=cs.id
  INNER JOIN
    course_dim c ON cs.course_id=c.id
  INNER JOIN
    role_dim r ON cse.role_id=r.id
  WHERE
    cse.workflow_state in ('active', 'completed')
    AND r.base_role_type = 'StudentEnrollment'
    AND c.workflow_state in ('available', 'completed')
    AND c.canvas_id IN ($course_str)
  GROUP BY
    c.canvas_id
    , c.name
    , c.code
END_OF_SQL

  my $sth = $db->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {

    my $course_canvas_id = $row->{course_canvas_id};
    my $course_data      = $udw_course_data->{$course_canvas_id};

    # Course enrollments – needs to be rounded
    # to the nearest 10.
    $course_data->{actual_student_enrollments}  = $row->{count_student_enrollments};
    $course_data->{rounded_student_enrollments} = nearest_floor(10, $row->{count_student_enrollments});

    # Course name and level.
    $course_data->{name}         = $row->{course_name};
    $course_data->{course_level} = undef;

    ## This is not elegant, but we parse the course code
    ## for the course subject and number.
    if( $institution eq 'iu' ) {

      ## Parse the code for a subject & number; we'll use this
      ## as the basis to compute the course level.
      if( $row->{course_code} =~ m|(\w\w\d\d)-(\w+)-(\w+)-(.+)-(.*)| ) {

        my $course_subject = $3 if $3 ne '';
        my $course_number  = $4 if $4 ne '';

        ## Use the course subject to identify the course category.
        if( defined $mapping_data->{$course_subject} ) {
          $course_data->{course_category} =
            $mapping_data->{$course_subject};
        } else {
          warn "No course category mapping for course subject: $course_subject\n";
        }

        ## Use the cours enumber to generate a course level, per the
        ## requiremens of the research.
        ##
        ## Case: IU Course number.
        my $course_level = undef;

        if($course_number =~ m|^[a-zA-Z]*(\d)\d\d|) {
          $course_data->{course_level} = 100 * $1;

        ## Case: IUPUI Course number.
        } elsif( $course_number =~ m|(\d\d\d\d)\d| ) {
          $course_data->{course_level} = $1;

        } else {
          warn "Unparseable course number: -$course_number-\n";
        }
      } else {
        warn "Failed to parse an IU course code for the course number\n";
      }
    }
  }

  print "... done\n";

  $sth->finish;

  return 1;
}

sub load_udw_course_assignment_count($$$$) {
  my($config, $db, $institution, $udw_course_data) = @_;

  print "Getting course-assignment data...";

  ## Unique set of course ids for the academic term.
  my $course_str =
    join(',',
      map { $_ + $config->{$institution}->{canvas_global} } keys $udw_course_data
    );

  ## Submissions type conditional for SQL.
  my $submissions_type_sql =
    &submission_types_allowed_sql($config, $institution, 'a.submission_types');

  ## To do:
  ##   * Handle case with no courses.

  ###############################################
  ## Count all of the assignments in the course.
  my $query = <<"END_OF_SQL";
  SELECT
    a.course_id course_global_canvas_id
    , count(a.id) count_assignments
  FROM
    assignment_dim a
  WHERE
    a.workflow_state = 'published'
    AND a.due_at IS NOT NULL
    AND a.course_id IN ($course_str)
    AND $submissions_type_sql
  GROUP BY
    a.course_id
END_OF_SQL

  my $sth = $db->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {

    my $course_canvas_id =
      $row->{course_global_canvas_id} -
      $config->{$institution}->{canvas_global};

    $udw_course_data->{$course_canvas_id}->{actual_count_assignments}  = $row->{count_assignments};
    $udw_course_data->{$course_canvas_id}->{rounded_count_assignments} = nearest_floor(10, $row->{count_assignments});
  }

  print "... done\n";

  $sth->finish;

  return 1;
}

sub load_udw_course_assignment_modules($$$$) {
  my($config, $db, $institution, $udw_course_data) = @_;

  print "Getting assignment-module item data...";

  ## Unique set of course ids for the academic term.
  my $course_str =
    join(',',
      map { $_ + $config->{$institution}->{canvas_global} } keys $udw_course_data
    );

  ## Submissions type conditional for SQL.
  my $submissions_type_sql =
    &submission_types_allowed_sql($config, $institution, 'a.submission_types');

  ## To do:
  ##   * Handle case with no courses.

  ###############################################
  ## Determine whether at least one assignment
  ## is in a module item.
  my $query = <<"END_OF_SQL";
SELECT
  a.course_id course_global_canvas_id
  , mi.id
  , CASE
    WHEN mi.id > 0 THEN 1
    ELSE 0
    END is_in_module_item
FROM assignment_dim a
LEFT JOIN
  module_item_dim mi ON a.id=mi.assignment_id
WHERE
  a.workflow_state = 'published'
  AND a.due_at IS NOT NULL
  AND a.course_id IN ($course_str)
  AND $submissions_type_sql
  AND mi.workflow_state = 'active'
END_OF_SQL

  my $sth = $db->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {

    my $course_canvas_id =
      $row->{course_global_canvas_id} -
      $config->{$institution}->{canvas_global};

    $udw_course_data->{$course_canvas_id}->{assignments_in_modules} = 1
      if $row->{is_in_module_item} == 1;
  }

  print "... done\n";

  return 1;
}

sub load_udw_course_assignment_discussions($$$$) {
  my($config, $db, $institution, $udw_course_data) = @_;

  print "Getting assignment-discussion stats...";

  ## Unique set of course ids for the academic term.
  my $course_str =
    join(',',
      map { $_ + $config->{$institution}->{canvas_global} } keys $udw_course_data
    );

  ## Submissions type conditional for SQL.
  my $submissions_type_sql =
    &submission_types_allowed_sql($config, $institution, 'a.submission_types');

  ## To do:
  ##   * Handle case with no courses.

  ################################################s
  ## Determine the percentage of assignments that
  ## are discussions.
  my $query = <<"END_OF_SQL";
SELECT
  d.course_global_canvas_id course_global_canvas_id
  , COUNT(d.assignment_canvas_id) num_assignments
  , SUM(d.is_discussion_assignment) num_discussion_assignments
  , num_discussion_assignments::decimal / num_assignments percentage_discussion_assignments
FROM (
  SELECT
    a.course_id course_global_canvas_id
    , a.id assignment_canvas_id
    , CASE
      WHEN def.assignment_id > 0 THEN 1
      ELSE 0
      END is_discussion_assignment
  FROM
    assignment_dim a
  LEFT JOIN
    discussion_topic_fact def ON a.id=def.assignment_id
  INNER JOIN
    discussion_topic_dim ded ON def.discussion_topic_id=ded.id
  WHERE
  a.workflow_state = 'published'
  AND a.due_at IS NOT NULL
  AND a.course_id IN ($course_str)
  AND $submissions_type_sql
  AND ded.workflow_state = 'active'
) d
GROUP BY
  d.course_global_canvas_id
;
END_OF_SQL

  my $sth = $db->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {

    my $course_canvas_id =
      $row->{course_global_canvas_id} -
      $config->{$institution}->{canvas_global};

    $udw_course_data->{$course_canvas_id}->{percentage_discussion_assignments} =
      $row->{percentage_discussion_assignments};
  }

  print "... done\n";

  return 1;
}

sub load_udw_course_assignment_quizzes($$$$) {
  my($config, $db, $institution, $udw_course_data) = @_;

  print "Getting assignment-quizzes stats...";

  ## Unique set of course ids for the academic term.
  my $course_str =
    join(',',
      map { $_ + $config->{$institution}->{canvas_global} } keys $udw_course_data
    );

  ## Submissions type conditional for SQL.
  my $submissions_type_sql =
    &submission_types_allowed_sql($config, $institution, 'a.submission_types');

  ## To do:
  ##   * Handle case with no courses.

  ################################################s
  ## Determine the percentage of assignments that
  ## are quizzes.
  my $query = <<"END_OF_SQL";
SELECT
  d.course_global_canvas_id course_global_canvas_id
  , COUNT(d.assignment_canvas_id) num_assignments
  , SUM(d.is_quiz_assignment) num_quiz_assignments
  , num_quiz_assignments::decimal / num_assignments percentage_quiz_assignments
FROM (
  SELECT
    a.course_id course_global_canvas_id
    , a.id assignment_canvas_id
    , CASE
      WHEN q.id > 0 THEN 1
      ELSE 0
      END is_quiz_assignment
  FROM
    assignment_dim a
  LEFT JOIN
    quiz_dim q ON a.id=q.assignment_id
  WHERE
    a.workflow_state = 'published'
    AND a.due_at IS NOT NULL
    AND a.course_id IN ($course_str)
    AND $submissions_type_sql
    AND q.workflow_state = 'published'
  ORDER BY a.course_id ASC
) d
GROUP BY
  d.course_global_canvas_id
;
END_OF_SQL

  my $sth = $db->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {

    my $course_canvas_id =
      $row->{course_global_canvas_id} -
      $config->{$institution}->{canvas_global};

    $udw_course_data->{$course_canvas_id}->{percentage_quiz_assignments} =
      $row->{percentage_quiz_assignments};
  }

  print "... done\n";

  return 1;
}

sub load_udw_assignment_data($$$$) {
  my($config, $db, $institution, $udw_course_data) = @_;
  my $data = {};

  my $hs = HTML::Strip->new();

  print "Getting assignment data...";

  ## Set default values for every course in the study.
  ## In case there are any courses without assignments,
  ## we still have "values" for them.
  foreach my $course_canvas_id (keys $udw_course_data) {
    $data->{$course_canvas_id} = {
      assignments                     => {},
      total_count_assignments         => 0,
      total_count_words_description   => 0,
      total_count_words_title         => 0,
      average_count_words_description => 0,
      average_count_words_title       => 0,
    } unless defined $data->{$course_canvas_id};
  }

  ## Grab all of the course sections from
  ## our enrollments data. We'll then go fetch all
  ## of the data for those assignments.
  my $course_str =
    join(',',
      map { $_ + $config->{$institution}->{canvas_global} } keys $udw_course_data
    );

  ## Submissions type conditional for SQL.
  my $submissions_type_sql =
    &submission_types_allowed_sql($config, $institution, 'a.submission_types');

  ## Now we get all of the assignments for these courses.
  ## Get all of the quizzes in this course
  my $query = <<"END_OF_SQL";
SELECT
  a.course_id course_global_canvas_id
  , a.id assignment_canvas_id
  , a.title assignment_title
  , a.description assignment_description
  , a.due_at due_date
FROM assignment_dim a
WHERE
  a.workflow_state = 'published'
  AND a.due_at IS NOT NULL
  AND a.course_id IN ($course_str)
  AND $submissions_type_sql
END_OF_SQL

  my $sth = $db->prepare($query);
     $sth->execute;

  while(my $row = $sth->fetchrow_hashref) {

    my $course_canvas_id =
      $row->{course_global_canvas_id} -
      $config->{$institution}->{canvas_global};

    my $assignment_canvas_id = $row->{assignment_canvas_id};
    my $course_data          = $data->{$course_canvas_id};

    #########################################
    ## Extract and clean the assignment data.
    my $description = $row->{assignment_description} ? $row->{assignment_description} : '';
    my $title       = $row->{assignment_title}       ? $row->{assignment_title} : '';

    my $description_clean = $hs->parse($description);
    my $title_clean       = $hs->parse($title);

    my $due_date     = undef;

    my $due_date_raw = $row->{due_date} ? $row->{due_date} : '';
       $due_date_raw = &clean($due_date_raw);

    if( $due_date_raw && $due_date_raw ne '' ) {
      $due_date = &tp($due_date_raw);
    }

    ######################################
    ## Set the cleaned up assignment data.
    $course_data->{assignments}->{$assignment_canvas_id} = {
      due_date                => $due_date,
      description             => $description_clean,
      count_words_description => scalar(split(/\s+/, $description_clean)),
      title                   => $title,
      count_words_title       => scalar(split(/\s+/, $title_clean))
    } unless defined $course_data->{assignments}->{$assignment_canvas_id};

    ##############################
    ## Update the course stats.
    ##
    ## Total count of assignments in the course.
    $course_data->{total_count_assignments}++;

    ## Description average word length.
    $course_data->{total_count_words_description} +=
      $course_data->{assignments}->{$assignment_canvas_id}->{count_words_description};

    $course_data->{average_count_words_description} =
      $course_data->{total_count_words_description} /
        scalar($course_data->{total_count_assignments});

    ## Title average word length.
    $course_data->{total_count_words_title} +=
      $course_data->{assignments}->{$assignment_canvas_id}->{count_words_title};

    $course_data->{average_count_words_title} =
      $course_data->{total_count_words_title} /
        scalar($course_data->{total_count_assignments});
  }

  print "... done\n";

  $hs->eof;

  return $data;
}

sub big_query_event_query($) {
  my($udw_course_data) = @_;

  my $course_str = join(', ', map { "'$_'" } keys $udw_course_data);

  print "\n\n";
  print "Run the following query against the UDP Event store to generate the events we'll proces for the analyse.\n";
  print "----\n\n";

  print <<"END_OF_SQL";
SELECT
  -- Event time & session
  event_time event_time
  , JSON_EXTRACT_SCALAR(event, "$[session][id]") AS session_id

  -- Event basics: type, action
  , type event_type
  , action event_action

  -- User information
  , actor_id user_canvas_id
  , JSON_EXTRACT_SCALAR(event, "$[actor][type]") AS actor_type

  -- Event's object information
  , JSON_EXTRACT_SCALAR(event, "$[object][id]") AS object_id
  , JSON_EXTRACT_SCALAR(event, "$[object][name]") AS object_name
  , JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][asset_type]") AS object_asset_type
  , JSON_EXTRACT_SCALAR(event, "$[object][extensions]['com.instructure.canvas'][asset_subtype]") AS object_asset_subtype

  -- In case of an action in relation to a submission,
  -- the assingee is the user_id of the person who submitted
  -- the assignment; the assignable is the assignment_id.
  , JSON_EXTRACT_SCALAR(event, "$[object][assignee][id]") AS assignee_id
  , JSON_EXTRACT_SCALAR(event, "$[object][assignable][id]") AS assignable_id

  -- Generated objects. The object is almost always the same
  -- as the generated.
  , JSON_EXTRACT_SCALAR(event, "$[generated][id]") AS generated_id

  -- Enrollment information
  , course_offering_id course_canvas_id
  , JSON_EXTRACT_SCALAR(event, "$[group][id]") AS group_id
  , JSON_EXTRACT_SCALAR(event, "$[membership][id]") AS membership_id
  , JSON_EXTRACT_SCALAR(event, "$[membership][member][id]") AS membership_member_id
  , JSON_EXTRACT_SCALAR(event, "$[membership][organization][id]") AS membership_organization_id
  , JSON_EXTRACT_SCALAR(event, "$[membership][roles][0]") AS role
  , ed_app
FROM
  event_store.events
WHERE
  event_time >= '2019-08-26'
  AND event_time <= '2019-12-13'
  AND JSON_EXTRACT_SCALAR(event, "$[object][id]") is not null
  AND JSON_EXTRACT_SCALAR(event, "$[actor][type]") = 'Person'
  AND
  (
    ed_app LIKE '\%instructure%'
    OR ed_app LIKE '\%canvas%'
  )
  AND course_offering_id IN ($course_str) # list of course canvas_id values
ORDER BY
  event_time ASC
;
END_OF_SQL

  print "\n----\n\n";

  return 1;
}

sub submission_types_allowed_sql($$$) {
  my($config, $institution, $column) = @_;

  my $submission_types_allowed =
    $config->{$institution}->{submission_types_allowed};

  my $x =
    '(' .
    join(' OR ', map { " $column like '\%$_\%' " } @$submission_types_allowed ) .
    ')';

  return $x;
}

################################################################################
################################################################################
##
## Analyze data about sections, students, and quizzes from the
## UDW and the UDP Context store before using it.
##
sub analyze_loaded_data($$$) {
  my(
    $fh_log,
    $survey_data,
    $udw_user_data,
    $udw_course_data,
    $udw_assigment_data
  ) = @_;

  &print_header_2($fh_log, 'Course data');

  my @course_ids = keys $udw_course_data;

  foreach my $course_id (@course_ids) {

    print $fh_log "\n\n";
    print $fh_log "Course: $course_id\n";

    print $fh_log "Name: ", $udw_course_data->{$course_id}->{name}, "\n";
    print $fh_log "Actual enrollments: ", $udw_course_data->{$course_id}->{actual_student_enrollments}, "\n";
    print $fh_log "Rounded enrollments: ", $udw_course_data->{$course_id}->{rounded_student_enrollments}, "\n";
    print $fh_log "Course level: ", $udw_course_data->{$course_id}->{course_level}, "\n";
    print $fh_log "Course category: ", $udw_course_data->{$course_id}->{course_category}, "\n";
    print $fh_log "Actual count assignments: ", $udw_course_data->{$course_id}->{actual_count_assignments}, "\n";
    print $fh_log "Rounded count assignments: ", $udw_course_data->{$course_id}->{rounded_count_assignments}, "\n";
    print $fh_log "Count assignments in modules: ", $udw_course_data->{$course_id}->{assignments_in_modules}, "\n";
    print $fh_log "Percentage of assignments that are discussion: ", $udw_course_data->{$course_id}->{percentage_discussion_assignments}, "\n";
    print $fh_log "Percentage of assignments that are quiz: ", $udw_course_data->{$course_id}->{percentage_quiz_assignments}, "\n";

  }

  ## Compare enrollment counts in each course
  ## with the sum of students you have for each
  ## course on record. They should be the same.
  my %local_course_enrollments = ();
  my @user_canvas_ids = keys $udw_user_data;
  foreach my $user_canvas_id (@user_canvas_ids) {

    my $course_canvas_ids = $udw_user_data->{$user_canvas_id}->{courses};

    foreach my $course_canvas_id (@$course_canvas_ids) {
      $local_course_enrollments{$course_canvas_id} = 0
        if ! $local_course_enrollments{$course_canvas_id};

      $local_course_enrollments{$course_canvas_id}++;
    }
  }

  foreach my $local_course_id (keys %local_course_enrollments) {
    print "Course $local_course_id ";
    print "(counted: $local_course_enrollments{$local_course_id};";
    print " sourced: $udw_course_data->{$local_course_id}->{actual_student_enrollments})";
    print "\n";
  }

  return 1;
}

sub set_event_user_data($$$) {
  my($fh_log, $udw_user_data, $udw_course_data, $udw_assigment_data) = @_;

  ## The governing data structure to track our
  ## user-course-assignment data.
  my $data = {};

  &print_header_1($fh_log, "Create data structure");

  ## If we haven't been given and valid user/course
  ## data, then we should die.
  die
    'Undefined value given for user/course data. ',
    "There's nothing for me to do.\n"
    unless $udw_user_data;

  ## Iterate over every user participating in the survey,
  ## and about whom we're going to generate some metrics.
  foreach my $user_id (keys $udw_user_data) {

    ## Fetch the courses that the student was enrolled in
    ## for the particular term.
    my $courses = $udw_user_data->{$user_id}->{courses};

    ## Bail to the next student if there are no courses
    ## that this student was enrolled in.
    next unless $courses;

    ## Iterate over each course that the student was
    ## enrolled in during the academic term.
    foreach my $course_id (@$courses) {

      ## Set up a data structure for this student's enrollment
      ## in this course.
      $data->{$user_id}->{$course_id} = {
        ## Track every session in the course during
        ## the academic term.
        sessions        => {},

        ## Count the total number of pageviews in the course
        ## during the academic term.
        count_pageviews => 0,

        ## Track every assignment and the user's submissions
        ## for that assignment.
        assignments     => {}
      };

      ## Fetch all of the assignments in the course that the
      ## student was enrolled in. Note: there is no guarantee
      ## that these assignments were always assigned to the
      ## student. We're using all of the assignments in the
      ## course. Some of them may have been written and assigned
      ## for a subset of students in the course's sections. We
      ## will not care about that here.
      my $assignments = $udw_assigment_data->{$course_id}->{assignments};

      ## Bail to the next course if there are no assignments
      ## in this one.
      next unless $assignments;

      foreach my $assignemnt_id (keys $assignments) {

        $data->{$user_id}->{$course_id}->{assignments}->{$assignemnt_id} = {

          ## The due date for the assignemnt. We'll ignore, for
          ## now, that there may be assignment overrides that affect
          ## this particular student.
          due_date         => undef,

          ## The date that the student first visited
          ## the assignment.
          first_navigation => undef,

          ## Each submission needs to itself be a data structure
          ## that captures: (1) sequence (first, second, third submission)
          ## (2) timestamp, (3) id. This will be used to compute
          ## "time_between_access_and_submission.""
          submissions => []
        };
      }
    }
  }

  return $data;
}

################################################################################
#################### Process events ############################################

sub process_events($$) {
  my($fh_log, $events, $event_user_data) = @_;

  ## To crawl the event data.
  my $match  = 0;
  my $index  = 0;
  my @events = @$events;

  foreach my $event ( @events ) {

    ## Skip if we don't have a person-based event.
    next if $event->{actor_type} ne 'Person';

    ## Extract some useful data.
    my $event_time     = $event->{event_time};
    my $event_time_tp  = &tp($event_time);

    my $user_canvas_id       = $event->{user_canvas_id};

    my $event_type           = $event->{event_type};
    my $event_action         = $event->{event_action};

    my $object_type          = $event->{object_type};
    my $object_asset_type    = $event->{object_asset_type};
    my $object_asset_subtype = $event->{object_asset_subtype};

    my $assignable_id   = $event->{assignable_id};
    my $assignable_type = $event->{assignable_type};

    my $session_id       = $event->{session_id};
    my $object_id        = $event->{object_id};
    my $course_canvas_id = $event->{course_canvas_id};

    ## Here, we need to bump up the student's number
    ## of pageviews in the course.
    next if ! $event_user_data->{$user_canvas_id};
    next if ! $event_user_data->{$user_canvas_id}->{$course_canvas_id};

    my $user_course_data =
      $event_user_data->{$user_canvas_id}->{$course_canvas_id};

    ############################################################################
    #################### Update our session information. #######################
    ############################################################################
    if( defined $session_id && $session_id ne '' ) {

      ## Get the session hash for the user-course.
      my $session_data = $user_course_data->{sessions};

      ## Create a structure to capture the event times
      ## for behaviors of this user with the course if
      ## non yet exists.
      unless( $session_data->{$session_id} ) {
        $session_data->{$session_id} = {
          first    => $event_time_tp,
          last     => $event_time_tp,
          events   => 0
        };
      }

      ## Do we have an earlier event or a later event
      ## than our current mix max?
      $session_data->{$session_id}->{first} = $event_time_tp
        if $event_time_tp < $session_data->{$session_id}->{first};

      $session_data->{$session_id}->{last} = $event_time_tp
        if $event_time_tp > $session_data->{$session_id}->{last};

      ## Increment number of user events in the sessions.
      $session_data->{$session_id}->{events}++;
    }

    ############################################################################
    #################### Act depending on the event type #######################
    ############################################################################

    my $user_assignments_data = $user_course_data->{assignments};

    #########################################################
    ## Did the user have a generic navigation event in the
    ## course? If so, we up the number of pageviews for the
    ## user in the course.
    if( $event_type   eq 'NavigationEvent' &&
        $event_action eq 'NavigatedTo'     &&
        $object_type  eq 'course'          ) {

      ## Increment page view count.
      $user_course_data->{count_pageviews}++;

      $match++;

    ########################################
    ## Did the user visit and assignment? ##
    } elsif( $event_type   eq 'NavigationEvent' &&
             $event_action eq 'NavigatedTo'     &&
             $object_type  eq 'assignment'      &&
             $object_id    ne ''                )  {

      if( defined $user_assignments_data->{$object_id} ) {

        ## If there is no first navigation value, or if there is one but the
        ## event timestamp we're processing is earlier than the current first
        ## nvigation value, then we set the first navigation value.
        $user_assignments_data->{$object_id}->{first_navigation} = $event_time_tp
          if
            ! defined $user_assignments_data->{$object_id}->{first_navigation} ||
            $event_time_tp < $user_assignments_data->{$object_id}->{first_navigation};

        print $fh_log "[good] [assignment visit] Set an assignment first visit for u: $user_canvas_id c: $course_canvas_id a: $object_id.\n";

      } else {
        print $fh_log "[warn] [assignment visit] No data structure for user $user_canvas_id and course $course_canvas_id assignment $object_id.\n";
      }

  #########################################################
  ## Did the user submit an assignment?
  ##
  } elsif( $event_type      eq 'AssignableEvent' &&
           $event_action    eq 'Submitted'     &&
           $object_type     eq 'submission'   ||
           $assignable_type eq 'assignment'   ) {

      if( defined $user_assignments_data->{$assignable_id} ) {

        my $submissions = $user_assignments_data->{$assignable_id}->{submissions};

        push($submissions, $event_time_tp);

        print $fh_log "[good] [submission] Got a submission for $assignable_id.\n";

      } else {
        print $fh_log "[warn] [submission] No data structure for assignment $assignable_id.\n";
      }
    }

    $index++;
  }

  print "Events: $index\n";

  return 1;
}

sub or_empty($) {
  my($x) = @_;

  return $x
    if(defined $x && $x ne '' );

  return '';
}

################################################################################
#################### Generate output files #####################################

sub generate_student_results($$$) {
  my ($fh_log, $institution, $o_file, $survey_data) = @_;

  open
    my $fh,
    ">:encoding(utf8)",
    $o_file
      or die "$o_file: $!";

  ## Header row for CSV output.
  $csv->print($fh, [
    'hashed_user_id'
  ]);

  print $fh "\n";

  foreach my $user_canvas_id (keys $survey_data) {
    my $user_hash_id  = md5_hex($user_canvas_id);

    $csv->print($fh, [
      $user_hash_id,
    ]);

    print $fh "\n";
  }

  close($fh);

  return 1;
}

sub generate_course_results($$$) {
  my($fh_log, $institution, $o_file, $udw_course_data, $udw_assigment_data) = @_;

  open
    my $fh,
    ">:encoding(utf8)",
    $o_file
      or die "$o_file: $!";

  ## Header row for CSV output.
  $csv->print($fh, [
    'hashed_course_id',
    'total_enrollments',
    'total_assignments',
    'face_to_face',
    'course_length',
    'course_subject_category',
    'course_level',
    'assignments_in_modules',
    'average_words_in_assignment_desc',
    'average_words_in_assignment_title',
    'percent_of_quiz_assignments',
    'percent_of_discussion_assignments'
  ]);

  print $fh "\n";

  foreach my $course_id (keys $udw_course_data) {

    my $course_data     = $udw_course_data->{$course_id};
    my $assignment_data = $udw_assigment_data->{$course_id};
    my $course_hash_id  = md5_hex($course_id);

    my $rounded_enrollments     = &or_empty($course_data->{rounded_student_enrollments});
    my $rounded_assignments     = &or_empty($course_data->{rounded_count_assignments});
    my $course_subject_category = &or_empty($course_data->{course_category});
    my $course_level            = &or_empty($course_data->{course_level});
    my $assignments_in_modules  = &or_empty($course_data->{assignments_in_modules});

    my $percent_of_quiz_assignments       = &or_empty($course_data->{percentage_quiz_assignments});
    my $percent_of_discussion_assignments = &or_empty($course_data->{percentage_discussion_assignments});

    $csv->print($fh, [
      $course_hash_id,
      $rounded_enrollments,
      $rounded_assignments,
      1, # face_to_face
      1, # course_length
      $course_subject_category,
      $course_level,
      $assignments_in_modules,
      $assignment_data->{average_count_words_description},
      $assignment_data->{average_count_words_title},
      $percent_of_quiz_assignments,
      $percent_of_discussion_assignments
    ]);

    print $fh "\n";
  }

  close($fh);

  return 1;
}

sub generate_student_course_results($$$$$) {
  my($fh_log, $institution, $o_file, $survey_data, $event_user_data) = @_;

  open
    my $fh,
    ">:encoding(utf8)",
    $o_file
      or die "$o_file: $!";

  ## Header row for CSV output.
  $csv->print($fh, [
    'hashed_student_id',
    'hashed_course_id',
    'time_between_access_and_submissions',
    'time_between_access_and_deadline',
    'time_between_submission_and_deadline',
    'number_of_submissions',
    'number_of_sessions',
    'duration_of_sessions',
    'number_of_pageviews'
  ]);

  print $fh "\n";

  foreach my $user_canvas_id (keys $survey_data) {

    my $user_hash_id      = md5_hex($user_canvas_id);
    my @course_canvas_ids = keys $event_user_data->{$user_canvas_id};

    foreach my $course_canvas_id (@course_canvas_ids) {

      my $course_hash_id = md5_hex($course_canvas_id);
      my $z_scores       = $event_user_data->{$user_canvas_id}->{$course_canvas_id}->{zscores};

      ## Header row for CSV output.
      $csv->print($fh, [
        $user_hash_id,
        $course_hash_id,
        $z_scores->{time_between_first_access_and_first_submission},
        $z_scores->{time_between_first_access_and_due_date},
        $z_scores->{time_between_last_submission_and_due_date},
        $z_scores->{count_submissions},
        $z_scores->{count_sessions},
        $z_scores->{duration_sessions},
        $z_scores->{count_pageviews}
      ]);

      print $fh "\n";
    }
  }

  close($fh);

  return 1;
}

################################################################################
#################### Canvas Live Events functions ##############################
sub parse_canvas_id($) {
  my($id) = @_;
  my($x, $y);

  if($id && $id =~ m|^urn:instructure:canvas:([\w:]+):(\w+)$| ) {
    $x = $1;
    $y = $2;
  } else {
    $x = '';
    $y = '';
  }

  return ($x, $y);
}

sub load_events($$) {
  my($i_events, $institution) = @_;
  my @events = ();

  open(my $fh, '<', $i_events)
    or die "Couldn't open the events file: $i_events.\n";

  while (my $line = <$fh>) {
    if( $csv->parse($line) ) {

      my @fields = $csv->fields();

      my $event_time                 = &clean($fields[0]);
      my $session_id                 = &clean($fields[1]);

      my $event_type                 = &clean($fields[2]);
      my $event_action               = &clean($fields[3]);

      my $user_canvas_id             = &clean($fields[4]);
      my $actor_type                 = &clean($fields[5]);

      my $object_id                  = &clean($fields[6]);
      my $object_name                = &clean($fields[7]);
      my $object_asset_type          = &clean($fields[8]);
      my $object_asset_subtype       = &clean($fields[9]);
      my $assignee_id                = &clean($fields[10]);
      my $assignable_id              = &clean($fields[11]);

      my $generated_id               = &clean($fields[12]);

      my $course_canvas_id           = &clean($fields[13]);
      my $group_id                   = &clean($fields[14]);
      my $membership_id              = &clean($fields[15]);
      my $membership_member_id       = &clean($fields[16]);
      my $membership_organization_id = &clean($fields[17]);
      my $role                       = &clean($fields[18]);

      my $ed_ap                      = &clean($fields[19]);

      ## Round 1: Need to parse the identifiers.
      (my $session_type, $session_id) = &parse_canvas_id($session_id);

      ## Conditions that force us to skip this record
      ## and move on to the next event.
      next if ! $session_type || $session_type eq '';
      next if ! $event_type   || $event_type eq '';
      next if ! $event_action || $event_action eq '';
      next if ! $actor_type   || $actor_type ne 'Person';
      next if ! $role         || $role ne 'Learner';

      ## Round 2: Need to parse the Canvas Live Events identifiers.
      (my $object_type, $object_id)         = &parse_canvas_id($object_id);
      (my $assignee_type, $assignee_id)     = &parse_canvas_id($assignee_id);
      (my $assignable_type, $assignable_id) = &parse_canvas_id($assignable_id);
      (my $generated_type, $generated_id)   = &parse_canvas_id($generated_id);
      (my $group_type, $group_id)           = &parse_canvas_id($group_id);
      (my $membership_member_type, $membership_member_id) =
        &parse_canvas_id($membership_member_id);
      (my $membership_organization_type, $membership_organization_id) =
        &parse_canvas_id($membership_organization_id);

      ## Adjust any global Canvas identifiers into local Canvas identifiers.
      $membership_organization_id -= $config->{$institution}->{canvas_global};

      ## If we're not getting matches in certain of the data points,
      ## we should error out.
      die "Mismatch of course data: $course_canvas_id vs $membership_organization_id, course vs $membership_organization_type"
        if $course_canvas_id != $membership_organization_id or 'course' ne $membership_organization_type;

      my %data = (
        event_time       => $event_time,

        session_type     => $session_type,
        session_id       => $session_id,

        event_type       => $event_type,
        event_action     => $event_action,

        user_canvas_id   => $user_canvas_id,
        actor_type       => $actor_type,

        object_id            => $object_id,
        object_type          => $object_type,
        object_name          => $object_name,
        object_asset_type    => $object_asset_type,
        object_asset_subtype => $object_asset_subtype,

        assignee_id      => $assignee_id,
        assignee_type    => $assignee_type,

        assignable_id    => $assignable_id,
        assignable_type  => $assignable_type,

        generated_id     => $generated_id,
        generated_type   => $generated_type,

        course_canvas_id => $course_canvas_id,
        group_type       => $group_type,
        group_id         => $group_id,

        role => $role,
      );

      push @events, \%data;
    }
  }

  close($fh);

  return @events;
}


################################################################################
#################### File system functions #####################################
sub check_dir($) {
  my($x) = @_;

  unless(-d $x) {
    mkdir $x;
  }

  return 1;
}

################################################################################
#################### Time functions ############################################
sub tp($$) {
  my($x, $d) = @_;

  if( ! defined $x ) {
    die "Undefined variable in &tp() that came from $d\n";
  }

  $x =~ s/UTC/ /g;
  $x =~ s/T/ /g;
  $x =~ s/\..*$//g;
  $x = &clean($x);

  my $tp = Time::Piece->strptime($x, '%Y-%m-%d %H:%M:%S');

  return $tp;
}

################################################################################
#################### Logging functions #########################################
sub print_header_1($$) {
  my($fh, $t) = @_;
  my $l = length($t) + 1;

  print $fh "\n";
  print $fh '=' x $l;
  print $fh "\n$t\n";
  print $fh '=' x $l;
  print $fh "\n";

  return 1;
}

sub print_header_2($$) {
  my($fh, $t) = @_;
  my $l = length($t) + 1;

  print $fh "\n$t\n";
  print $fh '-' x $l;
  print $fh "\n";

  return 1;
}

################################################################################
#################### Miscellaneous #############################################
sub clean($) {
  my($x) = @_;

  return '' if ! $x;

  $x =~ s/^\s+//;
  $x =~ s/\s+$//;

  return $x;
}

sub push_unique($$) {
  my($array, $value) = @_;

  my @x = uniq @$array;
  $array = \@x;

  return 1;
}

################################################################################
#################### Database functions ########################################
sub connect_to_database {
  my($name, $host, $port, $user, $pass) = @_;

  my $dbh = DBI->connect("dbi:Pg:dbname=$name;host=$host;port=$port", $user, $pass)
    || die "Could not connect to database\n";

  $dbh->{AutoCommit} = 0;
  $dbh->{RaiseError} = 1;
  $dbh->do("SET TIME ZONE 'UTC'");

  return $dbh;
}

sub disconnect_from_database {
  my($dbh) = @_;

  $dbh->disconnect;

  return 1;
}
